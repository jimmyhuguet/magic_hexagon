
import itertools
import time
import multiprocessing
from math import prod

def print_progression(n, l, freq=1000):
    if n % freq == 0:
        print(f"{n}/{l}    {n*100/l:.2f}%             ", end="\r")

def find_ntet(tiles, n):
    """ Group all combination of n element in tiles by the sum of those elements: store them on global index goal_index """
    count = 0
    index = {}
    l =  prod(range(len(tiles), len(tiles)-n, -1)) / prod(range(1, n+1))
    for p in itertools.combinations(tiles, n):
        s = sum(p)
        count += 1
        print_progression(count, l, freq=100000)
        if s not in index:
            index[s] = []
        index[s].append(frozenset(p))
        
    print(f"Added {len(index)} {n}-tets                       ")
    return index


def find_restricted_ntet(tiles, n, restriction):
    """ Group all combination of n element in tiles by the sum of those elements: store them on global index goal_index """
    count = 0
    index = {}
    l = len(tiles) * (len(tiles) -1) *(len(tiles) - 2) / 6
    for p in itertools.combinations(tiles, n):
        s = sum(p)
        count += 1
        print_progression(count, l, freq=100000)
        if not restriction(s):
            continue
        if s not in index:
            index[s] = []
        index[s].append(frozenset(p))
        
    print(f"Added {len(index)} {n}-tets                       ")
    return index


def find_center(tiles, restriction, restriction_name, display, scount_min):
    count = 0
    tet3 = find_ntet(tiles, 3)
    tet5 = find_ntet(tiles, 5)
    processes = []

    tet5_keys = list(tet5.keys())
    tet5_keys.sort()
    for center in tiles:
        index = set() # index of unique center hexagon
        for s1 in tet5_keys:
            if center + s1 not in tet3:
                continue
            for t1 in tet5[s1]:
                for s2 in tet5_keys:
                    if center + s2 not in tet3:
                        continue
                    for t2 in tet5[s2]:
                        if len(t1 & t2) != 2:
                            continue
                        for s3 in tet5_keys:
                            if center + s3 not in tet3:
                                continue
                            for t3 in tet5[s3]:
                                if len(t1 & t3) != 0 or len(t2 & t3) != 2:
                                    continue
    for p in processes:
        if p is not None:
            p.join()

def prepare_display(l, s="  "):
    return [
            s*4 + s.join(l[0:4]) + s*4,
            s*3 + s.join(l[4:9]) + s*3,
            s*2 + s.join(l[9:15]) + s*2,
            s*1 + s.join(l[15:22]) + s*1,
            s*2 + s.join(l[22:28]) + s*2,
            s*3 + s.join(l[28:33]) + s*3,
            s*4 + s.join(l[33:37]) + s*4
        ]

def find_square_hex(max):
    def is_square(n):
        """ Return true if n is the square of an integer, false otherwise """
        return n >= 0 and n **0.5 % 1 == 0
    
    def display(s, color="white"):
        if len(s) != 37:
            raise Exception("Solution must be 37 positions")
        l = [f'{(int(i**0.5)): 3d}²' if is_square(i) else f'{i: 4d}' for i in s]
        print("\n".join(prepare_display(l, "   ")))

    tiles = [(i+1)**2 for i in range(max)]
    t0 = time.time()
    find_center(tiles, is_square, "square", display, scount_min=6)
    print(f"\ntook {time.time()-t0:.2f} seconds                       ")


# find_square_hex(50)

def find_6tet(tiles, restriction):
    """ Group all combination of n element in tiles by the sum of those elements: store them on global index goal_index """
    count = 0
    index = {}
    l = len(tiles) * (len(tiles) -1) / 2
    for i1, p1 in enumerate(tiles):
        for i2, p2 in enumerate(tiles[i1+1:]):
            count += 1
            print_progression(count, l, freq=1)
            for i3, p3 in enumerate(tiles[i1+i2+2:]):
                for i4, p4 in enumerate(tiles[i1+i2+i3+3:]):
                    for i5, p5 in enumerate(tiles[i1+i2+i3+i4+4:]):
                        for i6, p6 in enumerate(tiles[i1+i2+i3+i4+i5+5:]):
                            s = p1 + p2 + p3 + p4 + p5 + p6
                            if not restriction(s):
                                continue
                            if s not in index:
                                index[s] = []
                            index[s].append(frozenset([p1, p2, p3, p4, p5, p6]))
    print(f"Added {len(index)} {6}-tets groups                       ")
    return index

def find_5tet(tiles, restriction):
    count = 0
    index = []
    l = len(tiles) * (len(tiles) -1) / 2
    for i1, p1 in enumerate(tiles):
        for i2, p2 in enumerate(tiles[i1+1:]):
            count += 1
            print_progression(count, l, freq=1)
            for i3, p3 in enumerate(tiles[i1+i2+2:]):
                for i4, p4 in enumerate(tiles[i1+i2+i3+3:]):
                    for i5, p5 in enumerate(tiles[i1+i2+i3+i4+4:]):
                        s = p1 + p2 + p3 + p4 + p5
                        if not restriction(s):
                            continue
                        index.append(frozenset([p1, p2, p3, p4, p5]))
    print(f"Added {len(index)} {5}-tets groups                       ")
    return index


def find_2tet(tiles):
    """ Group all combination of n element in tiles by the sum of those elements: store them on global index goal_index """
    # count = 0
    index = {}
    for i1, p1 in enumerate(tiles):
        for i2, p2 in enumerate(tiles[i1+1:]):
            s = p1 + p2
            # count += 1
            # print_progression(count, 1000000)
            if s not in index:
                index[s] = []
            index[s].append(frozenset([p1, p2]))
    print(f"Added {len(index)} {2}-tets groups                       ")
    return index

def find_3tet(tiles):
    """ Group all combination of n element in tiles by the sum of those elements: store them on global index goal_index """
    # count = 0
    index = {}
    for i1, p1 in enumerate(tiles):
        for i2, p2 in enumerate(tiles[i1+1:]):
            for i3, p3 in enumerate(tiles[i1+i2+2:]):
                s = p1 + p2 + p3
                # count += 1
                # print_progression(count, 1000000)
                if s not in index:
                    index[s] = []
                index[s].append(frozenset([p1, p2, p3]))
    print(f"Added {len(index)} {3}-tets groups                       ")
    return index

def print_hex(phi, a, b, d, f):
    print(f"""
        
         ----  {f[2]:04d}  {f[3]:04d}  ----
      {f[1]:04d}  {d[1]:04d}  {b[1]:04d}  {d[2]:04d}  {f[4]:04d}
   {f[0]:04d}  {b[0]:04d}  {a[1]:04d}  {a[2]:04d}  {b[2]:04d}  {f[5]:04d}
----  {d[0]:04d}  {a[0]:04d}  {phi:04d}  {a[3]:04d}  {d[3]:04d}  ----
  {f[11]:04d}  {b[5]:04d}  {a[5]:04d}  {a[4]:04d}  {b[3]:04d}  {f[6]:04d}
    {f[10]:04d}  {d[5]:04d}  {b[4]:04d}  {d[4]:04d}  {f[7]:04d}
      ----  {f[9]:04d}  {f[8]:04d}  ----
""")

tiles = [i**2 for i in range(1,100)]
tset = set(tiles)
tet2 = find_2tet(tiles)

def restriction(s):
    return s in tet2

tet3 = find_3tet(tiles)
all_tet3 = []
for s in tet3:
    all_tet3 += tet3[s]
print("tet3:", len(all_tet3))
# tet6 = find_6tet([i**2 for i in range(1,20)], restriction)

# all_tet6 = []
# for s in tet6:
#     all_tet6 += tet6[s]
# print(len(all_tet6))


def find_exterior(used,phi,b1,b3,b5,a2a3,a4a5,a6a1,f1f6,f2f9,f5f10,t1,t2,t3):
    for x3 in tet3[b1+b3+b5]:
        if len(x3 & used) != 0:
            continue
        b2b4b6 = tuple(x3)
        used2 = used | x3

        for b2 in b2b4b6:
            b4b6 = tuple(set(b2b4b6) - set([b2]))
            for a1 in a6a1:
                a6 = a6a1[0] if a6a1[0] != a1 else a6a1[1]
                for a4 in a4a5:
                    a5 = a4a5[0] if a4a5[0] != a4 else a4a5[1]
                    t4 = set([b2, a2a3[0], a2a3[1], phi, a1, a4])

                    if sum(t4) not in tet2:
                        continue
                    for p4 in tet2[sum(t4)]:
                        if len(p4 & used2) != 0:
                            continue
                        f7f12 = tuple(p4)
                        used3 = used2 | p4

                        for b4 in b4b6:
                            b6 = b4b6[0] if b4b6[0] != b4 else b4b6[1]
                            for a2 in a2a3:
                                a3 = a2a3[0] if a2a3[0] != a2 else a2a3[1]
                                t5 = set([b4, a5, a4, phi, a3, a6])
                                if sum(t5) not in tet2:
                                    continue
                                for p5 in tet2[sum(t5)]:
                                    if len(p5 & used3) != 0:
                                        continue
                                    f4f11 = tuple(p5)
                                    used4 = used3 | p5

                                    t6 = set([b6, a6, a1, phi, a2, a5])
                                    if sum(t6) not in tet2:
                                        continue
                                    for p6 in tet2[sum(t6)]:
                                        if len(p6 & used4) != 0:
                                            continue
                                        f3f8 = tuple(p6)

                                        used5 = used4 | p6
                                        s = sum(t1 | t2 | t3) + phi

                                        for f1 in f1f6:
                                            f6 = f1f6[0] if f1f6[0] != f1 else f1f6[1]
                                            for f10 in f5f10:
                                                f5 = f5f10[0] if f5f10[0] != f10 else f5f10[1]
                                                if s-f1-f10-b6 not in tet2:
                                                    continue
                                                for p7 in tet2[s-f1-f10-b6]:
                                                    if len(p7 & used5) != 0:
                                                        continue
                                                    d1d6 = tuple(p7)
                                                    used6 = used5 | p7

                                                    for f3 in f3f8:
                                                        f8 = f3f8[0] if f3f8[0] != f3 else f3f8[1]
                                                        for f12 in f7f12:
                                                            f7 = f7f12[0] if f7f12[0] != f12 else f7f12[1]
                                                            for d1 in d1d6:
                                                                d6 = d1d6[0] if d1d6[0] != d1 else d1d6[1]
                                                                d2 = s-f3-f12-b1-d1
                                                                if d2 not in tset or d2 in used6:
                                                                    continue
                                                                used7 = used6 | set([d2])
                                                                for f2 in f2f9:
                                                                    f9 = f2f9[0] if f2f9[0] != f2 else f2f9[1]
                                                                    d3 = s-f2-f5-b2-d2
                                                                    if d3 not in tset or d3 in used7:
                                                                        continue

                                                                    used8 = used7 | set([d3])
                                                                    for f4 in f4f11:
                                                                        f11 = f4f11[0] if f4f11[0] != f4 else f4f11[1]
                                                                        d4 = s-f4-f7-b3-d3
                                                                        
                                                                        if d4 not in tset or d4 in used8:
                                                                            continue
                                                                        d5 = s-f6-f9-b4-d4
                                                                        used9 = used8 | set([d4])
                                                                        print(f"got a result with sum: {s} up to d4")
                                                                        print_hex(phi, [a1,a2,a3,a4,a5,a6], [b1,b2,b3,b4,b5,b6], [d1,d2,d3,d4,d5,d6], [f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12])
                                                                        if d5 not in tset or d5 in used9:
                                                                            continue
                                                                        used10 = used9 | set([d5])
                                                                        print("d5 is a square")
                                                                        return True
    return False

def find_solution():
    count = 0
    centers = [i**2 for i in range(1,20)]
    l = len(centers)
    for phi in centers:
        all_tet5 = find_5tet([i**2 for i in range(1,50) if i**2 != phi], lambda s: s+phi in tet2)
        count += 1
        for ti1, t1 in enumerate(all_tet5):
            flag = False
            for p1 in tet2[sum(t1)+phi]:
                if len(t1 & p1) != 0 or phi in p1:
                    continue
                flag = True
                break
            if not flag:
                continue
            for ti2, t2 in enumerate(all_tet5[ti1+1:]):
                print(f"phi: {phi}/{l} {phi*100/l:.2f}%       t1: {ti1}/{len(all_tet5)} {ti1*100/len(all_tet5):.2f}%        t2: {ti2}/{len(all_tet5[ti1+1:])} {ti2*100/len(all_tet5[ti1+1:]):.2f}%  ", end="\r")
                if len(t1 & t2) != 2:
                    continue
                flag = True
                for p2 in tet2[sum(t2)+phi]:
                    if len(t2 & p2) != 0 or len(t1 & p2) != 0 or phi in p2:
                        continue
                    flag = True
                    break
                if not flag:
                    continue
                for t3 in all_tet5[ti1+ti2+2:]:
                    if len(t1 & t3) != 2 or len(t2 & t3) != 2 or len(t1 & t2 & t3) != 0:
                        continue
                    for p1 in tet2[sum(t1)+phi]:
                        for p2 in tet2[sum(t2)+phi]:
                            for p3 in tet2[sum(t3)+phi]:
                                if len(t3 & p3) != 0 or len(p1 & p3) != 0 or len(p2 & p3) != 0 or len(t1 & p3) != 0 or len(t2 & p3) != 0 or phi in p3:
                                    continue
                                # print(t1, t2, t3, p1, p2, p3)
                                used = t1 | t2 | t3 | p1 | p2 | p3 | set([phi])
                                b1 = list(t1 - t2 - t3)[0]
                                b3 = list(t2 - t1 - t3)[0]
                                b5 = list(t3 - t1 - t2)[0]
                                a2a3 = tuple(t1 & t2 - set([phi]))
                                a4a5 = tuple(t2 & t3 - set([phi]))
                                a6a1 = tuple(t3 & t1 - set([phi]))
                                f1f6 = tuple(p3)
                                f2f9 = tuple(p2)
                                f5f10 = tuple(p1)

                                if find_exterior(used,phi,b1,b3,b5,a2a3,a4a5,a6a1,f1f6,f2f9,f5f10,t1,t2,t3):
                                    exit()

def find_fs(phi, a, b, tet2, r):

    return False



def find_solution():
    count = 0
    centers = [i**2 for i in range(1,20)]
    tet2 = find_2tet([i**2 for i in range(1,100)])
    l = len(centers)
    for phi in centers:
        r1 = set([i**2 for i in range(1,50) if i**2 != phi])
        for i1, a1 in enumerate(r1):
            r2 = r1 - set([a1])
            for i2, a2 in enumerate(r2):
                r3 = r2 - set([a2])
                for i3, a3 in enumerate(r3):
                    r4 = r3 - set([a3])
                    for i4, a4 in enumerate(r4):
                        r5 = r4 - set([a4])
                        for i5, b2 in enumerate(r5):
                            if phi+a1+a2+a3+a4+b2 not in tet2:
                                continue
                            r6 = r5 - set([b2])
                            for i6, a5 in enumerate(r6):
                                print(f"phi: {phi}/{len(centers)} {phi*100/len(centers):.2f}%  \
a1: {a1}/{len(r1)} {i1*100/len(r1):.2f}%  \
a2: {a2}/{len(r2)} {i2*100/len(r2):.2f}%  \
a3: {a3}/{len(r3)} {i3*100/len(r3):.2f}%  \
a4: {a4}/{len(r4)} {i4*100/len(r4):.2f}%  \
b2: {b2}/{len(r5)} {i5*100/len(r5):.2f}%  \
a5: {a5}/{len(r6)} {i6*100/len(r6):.2f}%  ", end="\r")
                                r7 = r6 - set([a5])
                                for b3 in r7:
                                    if phi+a2+a3+a4+a5+b3 not in tet2:
                                        continue
                                    r8 = r7 - set([b3])
                                    for a6 in r8:
                                        r9 = r8 - set([a6])
                                        for b4 in r9:
                                            if phi+a3+a4+a5+a6+b4 not in tet2:
                                                continue
                                            r10 = r9 - set([b4])
                                            for b5 in r10:
                                                if phi+a4+a5+a6+a1+b5 not in tet2:
                                                    continue
                                                r11 = r10 - set([b5])
                                                for b6 in r11:
                                                    if phi+a5+a6+a1+a2+b6 not in tet2:
                                                        continue
                                                    r12 = r11 - set([b6])
                                                    b1 = b2+b4+b6-b3-b5
                                                    if b1 not in r12 or phi+a6+a1+a2+a3+b1 not in tet2:
                                                        continue
                                                    if find_fs(phi, [a1,a2,a3,a4,a5,a6], [b1,b2,b3,b4,b5,b6], tet2, r12):
                                                        exit()
                                                    # print_hex(phi, [a1,a2,a3,a4,a5,a6], [b1,b2,b3,b4,b5,b6], [0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0,0,0,0])






find_solution()
