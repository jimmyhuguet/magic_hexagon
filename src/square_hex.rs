use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::BTreeSet;


fn prepare_solution_for_printing(s: &[i32; 19]) -> Vec<String> {
    if s.len() != 19 {
        panic!("Solution must be 19 positions");
    }
    let mut l: Vec<String> = s.iter().map(|&i| format!("{:02}", i)).collect();
    for i in 0..19 {
        if is_square(s[i]) {
            l[i] = format!("{:02}²", (s[i] as f64).sqrt() as i32);
        }
    }
    vec![
        format!("      {}  {}  {}", l[0], l[1], l[2]),
        format!("    {}  {}  {}  {}", l[3], l[4], l[5], l[6]),
        format!("  {}  {}  {}  {}  {}", l[7], l[8], l[9], l[10], l[11]),
        format!("    {}  {}  {}  {}", l[12], l[13], l[14], l[15]),
        format!("      {}  {}  {}", l[16], l[17], l[18]),
    ]
}

pub fn display_solution(s: &[i32; 19]) {
    if s.len() != 19 {
        panic!("Solution must be 19 positions");
    }
    for line in prepare_solution_for_printing(s).iter() {
        println!("{}", line);
    }
}

fn is_square(n: i32) -> bool {
    let s = (n as f64).sqrt();
    return s % 1.0 == 0.0;
}

pub fn find_3tet_with_given_center(center: i32, m4_0: &Vec<i32>, m4_1: &Vec<i32>) -> HashMap::<i32, HashSet<i32>> {
    fn fill_index(index: &mut HashMap::<i32, HashSet<i32>>, p2: i32, p3: i32)  {
        fn _inner_add(index: &mut HashMap::<i32, HashSet<i32>>, p2: i32, p3: i32) {
            if !index.contains_key(&p2) {
                let set = HashSet::<i32>::new();
                index.insert(p2, set);
            }
            if let Some(set) = index.get_mut(&p2) {
                set.insert(p3.try_into().unwrap());
            }
        }
        _inner_add(index, p2, p3);
        _inner_add(index, p3, p2);
    }

    let mut index = HashMap::<i32, HashSet::<i32>>::new();

    if center % 4 == 0 {
        for i2 in 0..m4_0.len() {
            let p2 = m4_0[i2];
            for p3 in m4_0[i2+1..].iter() {
                if center==p2 || center==*p3 {
                    continue;
                }
                let s = center + p2 + p3;
                if is_square(s) {
                    fill_index(&mut index, p2, *p3);
                }
            }
            for p3 in m4_1.iter() {
                if center==p2 || center==*p3 {
                    continue;
                }
                let s = center + p2 + p3;
                if (s as f64).sqrt() % 1.0 == 0.0 {
                    fill_index(&mut index, p2, *p3);
                }
            }
        }
    } else {
        for i2 in 0..m4_0.len() {
            let p2 = m4_0[i2];
            for p3 in m4_0[i2+1..].iter() {
                if center==p2 || center==*p3 {
                    continue;
                }
                let s = center + p2 + p3;
                if is_square(s) {
                    fill_index(&mut index, p2, *p3);
                }
            }
        }
    }

    return index;
}

pub fn solve_magic_hex(tiles: Vec<i32>, start_a: i32, end_a: Option<i32>) -> Vec<[i32; 19]> {
    let mut tiles_0 = Vec::new();
    let mut tiles_1 = Vec::new();
    let mut r0 = HashSet::<i32>::new();
    for t in tiles.iter() {
        r0.insert((*t).try_into().unwrap());
        if t % 4 == 0 {
            tiles_0.push(*t);
        } else {
            tiles_1.push(*t);
        }
    }
    for a in tiles.iter() {
        if (*a as f64).sqrt() < start_a as f64 {
            continue;
        }
        if let Some(end_a) = end_a {
            if (*a as f64).sqrt() > end_a as f64 {
                println!("A: {}² => Stopped.                       ", (*a as f64).sqrt());
                break;
            }
        }
        print!("A: {}² => Preparing index                                                 \r", (*a as f64).sqrt());
        let solution = solve_magic_hex_with_given_center(&r0, &tiles_0, &tiles_1, *a);
        if solution.len() > 0 {
            return solution;
        }
    }
    return Vec::new();
}

pub fn solve_magic_hex_with_given_center(r0: &HashSet<i32>, tiles_0: &Vec<i32>, tiles_1: &Vec<i32>, a: i32) -> Vec<[i32; 19]> {
    let index = find_3tet_with_given_center(a, tiles_0, tiles_1);
    let mut done_bc = BTreeSet::new();
    let mut b_keys: Vec<_> = index.keys().collect();
    b_keys.sort();
    for ib in 0..b_keys.len() {
        let b = *(b_keys[ib]);
        let mut c_keys: Vec<_> = index.get(&b).unwrap().iter().collect();
        c_keys.sort();
        for ic in 0..c_keys.len() {
            let c = *(c_keys[ic]);
            if done_bc.contains(&(b, c)) || done_bc.contains(&(c, b)) {
                continue;
            }
            print!("A: {}²,  B: {}²  ({}/{}),  C: {}²          \r", (a as f64).sqrt(), (b as f64).sqrt(), ib, b_keys.len(), (c as f64).sqrt());
            done_bc.insert((b, c));
            done_bc.insert((c, b));
            let abc = a+b+c;
            for ud in index.get(&c).unwrap().iter() {
                let d = *ud as i32;
                if done_bc.contains(&(c, d)) || done_bc.contains(&(d, c)) {
                    continue;
                }
                let acd = a+c+d;
                for ue in index.get(&d).unwrap().iter() {
                    let e = *ue as i32;
                    if done_bc.contains(&(d, e)) || done_bc.contains(&(e, d)) {
                        continue;
                    }
                    let ade = a+d+e;
                    for uf in index.get(&e).unwrap().iter() {
                        let f = *uf as i32;
                        if done_bc.contains(&(e, f)) || done_bc.contains(&(f, e)) {
                            continue;
                        }
                        let aef = a+e+f;
                        for ug in index.get(&f).unwrap().iter() {
                            let g = *ug as i32;
                            if done_bc.contains(&(f, g)) || done_bc.contains(&(g, f)) ||
                                done_bc.contains(&(b, g)) || done_bc.contains(&(g, b)) {
                                continue;
                            }
                            let abg = a+b+g;
                            if !is_square(abg) {
                                continue;
                            }
                            let agf = a+g+f;
                            let mut used = HashSet::<i32>::new();
                            used.insert(a.try_into().unwrap());
                            used.insert(b.try_into().unwrap());
                            used.insert(c.try_into().unwrap());
                            used.insert(d.try_into().unwrap());
                            used.insert(e.try_into().unwrap());
                            used.insert(f.try_into().unwrap());
                            used.insert(g.try_into().unwrap());
                            used.insert(abc.try_into().unwrap());
                            used.insert(acd.try_into().unwrap());
                            used.insert(ade.try_into().unwrap());
                            used.insert(aef.try_into().unwrap());
                            used.insert(abg.try_into().unwrap());
                            used.insert(agf.try_into().unwrap());
                            // if used.len() != 13 {
                            //     continue;
                            // }
                            for uh in r0.iter() {
                                let h = *uh as i32;
                                if used.contains(&h) {
                                    continue;
                                }
                                // let mut count = 6;
                                let abcdgh = a+b+c+d+g-h;
                                if !is_square(abcdgh) {
                                    continue;
                                }
                                let ehg = e+h-g;
                                if !is_square(ehg) {
                                    continue;
                                }
                                let abcfgh = a+b+c+f+g-h;
                                if !is_square(abcfgh) {
                                    continue;
                                }
                                let ehc = e+h-c;
                                if !is_square(ehc) {
                                    continue;
                                }
                                let acdfgh = a+c+d+f+g-h;
                                if !is_square(acdfgh) {
                                    continue;
                                }
                                // if count < 5 {
                                //     continue
                                // }
                                used.insert(h.try_into().unwrap());
                                used.insert(abcdgh.try_into().unwrap());
                                used.insert(ehg.try_into().unwrap());
                                used.insert(abcfgh.try_into().unwrap());
                                used.insert(ehc.try_into().unwrap());
                                used.insert(acdfgh.try_into().unwrap());
                                if used.len() < 16 {
                                    continue
                                }
                                println!("\n Count = {}, unique: {}", 6, used.len());
                                let solution: [i32; 19] = [
                                    h.try_into().unwrap(),
                                    aef.try_into().unwrap(),
                                    abcdgh.try_into().unwrap(),
                                    ade.try_into().unwrap(),
                                    b.try_into().unwrap(),
                                    c.try_into().unwrap(),
                                    agf.try_into().unwrap(),
                                    abcfgh.try_into().unwrap(),
                                    g.try_into().unwrap(),
                                    a.try_into().unwrap(),
                                    d.try_into().unwrap(),
                                    ehg.try_into().unwrap(),
                                    acd.try_into().unwrap(),
                                    f.try_into().unwrap(),
                                    e.try_into().unwrap(),
                                    abg.try_into().unwrap(),
                                    ehc.try_into().unwrap(),
                                    abc.try_into().unwrap(),
                                    acdfgh.try_into().unwrap()
                                ];
                                display_solution(&solution);
                                if used.len() == 19 {
                                    return vec![solution];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Vec::new();
}
