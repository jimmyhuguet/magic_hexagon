import time

def prepare_solution_for_printing(s):
    if len(s) != 19:
        raise Exception("Solution must be 19 positions")
    l = []
    for i in s:
        if i not in cube_roots_index:
            l.append(f'{i:02d}')
        else:
            l.append(f'{cube_roots_index[i]:02d}³')
    return ["      " + "  ".join(l[0:3]) + "      ",
            "    " + "  ".join(l[3:7]) + "    ",
            "  " + "  ".join(l[7:12]) + "  ",
            "    " + "  ".join(l[12:16]) + "    ",
            "      " + "  ".join(l[16:19]) + "      "
    ]

def display_solution(s):
    if len(s) != 19:
        raise Exception("Solution must be 19 positions")
    print("\n".join(prepare_solution_for_printing(s)))


def print_progression(n, l, freq=1000):
    if n % freq == 0:
        print(f"{n}/{l}    {n*100/l:.2f}%             ", end="\r")

def find_3tet_with_given_center(center, m9_0, m9_1, m9_8):
    
    def fill_index(index, center, p2, p3):
        def _inner_add(index, p2, p3):
            if p2 not in index:
                index[p2] = set()
            index[p2].add(p3)

        s = center + p2 + p3
        if is_cube(s):
            _inner_add(index, p2, p3)
            _inner_add(index, p3, p2)
    
    index = {}

    if center % 9 == 0:
        for i2, p2 in enumerate(m9_0):
            for p3 in m9_0[i2+1:]:
                fill_index(index, center, p2, p3)
            for p3 in m9_1:
                fill_index(index, center, p2, p3)
            for p3 in m9_8:
                fill_index(index, center, p2, p3)
        for i2, p2 in enumerate(m9_1):
            for p3 in m9_8:
                fill_index(index, center, p2, p3)
    elif center % 9 == 1:
        for i2, p2 in enumerate(m9_8):
            for p3 in m9_0:
                fill_index(index, center, p2, p3)
            for p3 in m9_1:
                fill_index(index, center, p2, p3)
            for p3 in m9_8[i2+1:]:
                fill_index(index, center, p2, p3)
        for i2, p2 in enumerate(m9_0):
            for p3 in m9_0[i2+1:]:
                fill_index(index, center, p2, p3)
    else:
        for i2, p2 in enumerate(m9_1):
            for p3 in m9_0:
                fill_index(index, center, p2, p3)
            for p3 in m9_1[i2+1:]:
                fill_index(index, center, p2, p3)
            for p3 in m9_8:
                fill_index(index, center, p2, p3)
        for i2, p2 in enumerate(m9_0):
            for p3 in m9_0[i2+1:]:
                fill_index(index, center, p2, p3)
    return index


all_cube = set([x**3 for x in range(-100000, 100000)])
cube_roots_index = {x**3:x for x in range(-100000, 100000)}
def is_cube(n):
    return n in all_cube

def solve_magic_hex_with_given_center(r0, tiles_0, tiles_1, tiles_8, A):
    index = find_3tet_with_given_center(A, list(tiles_0 - {A}), list(tiles_1 - {A}), list(tiles_8 - {A}))
    print(f"{A}: {len(index)}                                                     ")
    doneBC = set()
    for B in index:
        print(f"A: {cube_roots_index[A]}³,  B: {cube_roots_index[B]}³                                                ", end="\r")
        for C in index[B]:
            if (B,C) in doneBC or (C,B) in doneBC:
                continue
            print(f"A: {cube_roots_index[A]}³,  B: {cube_roots_index[B]}³,   C: {cube_roots_index[C]}³                 ", end="\r")
            doneBC.add((B,C))
            doneBC.add((C,B))
            ABC = A+B+C
            if not is_cube(ABC):
                continue
            for D in index[C]:
                if (C,D) in doneBC or (D,C) in doneBC:
                    continue
                ACD = A+C+D
                if not is_cube(ACD):
                    continue
                for E in index[D]:
                    if (D,E) in doneBC or (E,D) in doneBC:
                        continue
                    ADE = A+D+E
                    if not is_cube(ADE):
                        continue
                    for F in index[E]:
                        if (E,F) in doneBC or (F,E) in doneBC:
                            continue
                        AEF = A+E+F
                        if not is_cube(AEF):
                            continue
                        for G in index[B]:
                            if (G,B) in doneBC or (B,G) in doneBC or (G,F) in doneBC or (F,G) in doneBC:
                                continue
                            ABG = A+B+G
                            if not is_cube(ABG):
                                continue
                            AGF = A+G+F
                            if not is_cube(AGF):
                                continue
                            used = {A,B,C,D,E,F,G,ABC,ACD,ADE,AEF,ABG,AGF}
                            if len(used) != 13:
                                continue
                            remaining = r0 - used
                            for H in remaining:
                                count = 6
                                ABCDGH = A+B+C+D+G-H
                                if not is_cube(ABCDGH):
                                    count -= 1
                                EHG = E+H-G
                                if not is_cube(EHG):
                                    count -= 1
                                ABCFGH = A+B+C+F+G-H
                                if not is_cube(ABCFGH):
                                    count -= 1
                                EHC = E+H-C
                                if not is_cube(EHC):
                                    count -= 1
                                ACDFGH = A+C+D+F+G-H
                                if not is_cube(ACDFGH):
                                    count -= 1
                                # if count < 3:
                                #     continue
                                used = used | {H, ABCDGH, EHG, ABCFGH, EHC, ACDFGH}
                                if count < 6 and len(used) < 19:
                                    continue
                                print(f"\n Count = {count}, unique: {len(used)}")
                                display_solution([H, AEF, ABCDGH, ADE, B, C, AGF, ABCFGH, G, A, D, EHG, ACD, F, E, ABG, EHC, ABC, ACDFGH])
                                if count == 6 and len(used) == 19:
                                    return [H, AEF, ABCDGH, ADE, B, C, AGF, ABCFGH, G, A, D, EHG, ACD, F, E, ABG, EHC, ABC, ACDFGH]
    return []

def solve_magic_hex(tiles):
    tiles_0 = set()
    tiles_1 = set()
    tiles_8 = set()
    for t in tiles:
        if t % 9 == 0:
            tiles_0.add(t)
        elif t % 9 == 1:
            tiles_1.add(t)
        elif t % 9 == 8:
            tiles_8.add(t)
        else:
            raise Exception(f"Unexpected tile, {t} % 9 = {t%9}")
    r0 = frozenset(tiles)
    for A in tiles:
        print(f"A: {cube_roots_index[A]}³ => Preparing index                                                 ", end="\r")
        solution = solve_magic_hex_with_given_center(r0, tiles_0, tiles_1, tiles_8, A)
        if len(solution) > 0:
            return solution
    return []



def solve_cube_magic_hex():
    timing_number = 1
    tiles = [(i**3) for i in range(10000)]
    t0 = time.time()
    solution = []
    for i in range(timing_number):
        solution = solve_magic_hex(tiles)
    print(f"ran {timing_number} times: iteration took {(time.time()-t0)/timing_number:.2f} seconds on average\n")
    if len(solution) == 0:
        print("No solution found")
    else:
        display_solution(solution)


solve_cube_magic_hex()
