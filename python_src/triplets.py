import time



def print_progression(n, l, freq=1000):
    if n % freq == 0:
        print(f"{n}/{l}    {n*100/l:.2f}%             ", end="\r")

def classic_find_3tet(tiles, restriction):
    count = 0
    index = {}
    l = len(tiles)
    for i1, p1 in enumerate(tiles):
        count += 1
        print_progression(count, l, freq=1)
        for i2, p2 in enumerate(tiles[i1+1:]):
            si = p1+p2
            for p3 in tiles[i1+i2+2:]:
                s = si+p3
                if not restriction(s):
                    continue
                if s not in index:
                    index[s] = []
                index[s].append(frozenset((p1,p2,p3)))
        
    print(f"Added {len(index)} 3-3tets                       ")
    return index

def better_find_3tet(tiles):
    """ This methods find triplets of squares that sum to a square.
        It takes advantage of the fact that at most one of the three must be congruent to 1 modulo 4
        the rest being congruent to 0 modulo 4
        x => x^2 % 4
        0 => 0
        1 => 1
        2 => 0
        3 => 1

        0 + 0 + 0 => 0
        0 + 0 + 1 => 1
        0 + 1 + 1 => 2 which cannot be a square
        1 + 1 + 1 => 3 which cannot be a square
        """
    m4_0 = [] # tiles congruent to 0 modulo 4
    m4_1 = [] # tiles congruent to 1 modulo 4
    count = 0
    print("Grouping tiles by modulo 4")
    for t in tiles:
        if t % 4 == 0:
            m4_0.append(t)
        else:
            m4_1.append(t)
        count += 1
        print_progression(count, len(tiles), freq=1)

    m4_p0 = {} # pairs of tiles whose sum is congruent to 0 modulo 4
    print("Summing pairs of tiles congruent to 0 modulo 4")
    count = 0
    t = len(m4_0)*len(m4_0)/2
    for i1, p1 in enumerate(m4_0):
        for p2 in m4_0[i1+1:]:
            si = p1+p2
            if si not in m4_p0:
                m4_p0[si] = []
            m4_p0[p1+p2].append((p1,p2))
            count +=1
            print_progression(count, t, freq=1)

    index = {}
    print("Summing pairs whose sum is congruent to 0 modulo 4 with all tiles")
    # we only keep the triplets that actually are squares
    count = 0
    for s1, l1 in m4_p0.items():
        for p2 in tiles:
            s = s1 + p2
            if s**0.5 % 1 != 0:
                continue
            if s not in index:
                index[s] = set()
            for p1 in l1:
                if p2 == p1[0] or p2 == p1[1]:
                    continue
                index[s].add(frozenset((p1[0],p1[1],p2)))
        count += 1
        print_progression(count, len(m4_p0), freq=1)
           
    print("Removing sums that have no triplets") 
    all_keys = list(index.keys())
    for s in all_keys:
        if len(index[s]) == 0:
            del index[s]
    
    return index


def compare_new_and_old():
    tiles = [i**2 for i in range(500)]
    t0 = time.time()
    index_classic = classic_find_3tet(tiles, lambda n: n**0.5 % 1 == 0)
    print(f"Classic: {time.time()-t0}, len: {len(index_classic)}")
    t1 = time.time()
    index_better = better_find_3tet(tiles)
    print(f"Better: {time.time()-t1}, len: {len(index_better)}")
        
    classic_keys = set(index_classic.keys())
    better_keys = set(index_better.keys())
    if classic_keys != better_keys:
        print("Keys are different")
        print(classic_keys - better_keys)
        print(better_keys - classic_keys)
    else:
        print("Keys are the same")
        all_triplets_classic = set()
        all_triplets_better = set()
        for key in classic_keys:
            if len(index_classic[key]) != len(index_better[key]):
                print(f"Key {key} has different number of triplets")
                print(index_classic[key])
                print(index_better[key])
            all_triplets_classic.update(index_classic[key])
            all_triplets_better.update(index_better[key])
        print(len(all_triplets_classic), len(all_triplets_better))
        

    # print(index_classic.keys(), index_classic)
    # print(index_better.keys(), index_better)

def calculate_timing_progression():
    for i in range(20):
        tiles = [i**2 for i in range(i*100)]
        t0 = time.time()
        index = better_find_3tet(tiles)
        print(f"{i*100}: {time.time()-t0}")

# calculate_timing_progression()
index = better_find_3tet([i**2 for i in range(10000)])
