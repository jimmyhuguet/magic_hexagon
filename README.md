# About the project

This project's goal is to find a magic hexagon made of square numbers

The algorithm ground work was done in python, now the code is being converted to rust to boost performance and hopefully find a solution.

# Algorithms

Is can easily be showed that any magic hexagon can be seeded from only 8 value, the remaining 11 other values can be inferred from them

```
    H x x
   x B C x
  x G A D x
   x F E x  
    x x x
```

We can first calculate the magic sum of the magic hexagon (S) which is `S=2A+B+C+D+E+F+G` (note: S doesn't depend on H)

Then each the middle value of the outside lines (made of 3 values) can be computed

```
    H i x
   n B C j
  x G A D x
   m F E k  
    x l x

i = A+E+F
j = A+F+G
k = A+G+B
l = A+B+C
m = A+C+D
n = A+D+E
```

And finally we can compute the corner using H

```
    H x p
   x B C x
  u G A D q
   x F E x  
    t x r

p = S-i-H = 2A+B+C+D+E+F+G-A-E-F-H = A+B+C+D+G-H
u = A+B+C+F+G-H

q = S-p-j = E+H-G
t = E+H-C

r = S-t-l = S-q-k = S-A-B-E-H = A+C+D+F+G-H
```

Because of the outer lines middle, our triplets needs to be squares whose sum are a square  `x²+y²+z²=m²`

If we take these numbers mod 4 we can see there are only a limited subset of square triplet that can be in this form

```
mod 4
x => x²
0 => 0
1 => 1
2 => 0
3 => 1

x mod 4 = 0 | 1

0+0+0 = 0
0+0+1 = 1
0+1+1 = 2 <= cannot be a square
1+1+1 = 3 <= cannot be a square
```
**At most one** of the value in the triplet can be odd

To construct the center we need 6 triplet sharing a common center.

The number of triplets grows quite fast when we combine larger list of square, so in order to save on RAM, we only treat one center value at a time and recompute all the triplet for a given center (we will compute each triplet 3 time)

For a given center x either:
```
x is odd => we use all pair of even square 
x is even => 
    we use pairs composed of two even 
    we use pairs composed of one even and one odd
```

We iterated the inner values to maximize the number of test we can do in order to maximize early `continue` and minimizes the number of nested loops we have to go through

