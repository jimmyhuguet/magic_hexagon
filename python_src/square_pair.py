
import itertools
import time
import multiprocessing

def sec(t1, t2):
    """ Return the first element (assumed to be the only one) at the intersection of two set """
    return next(iter(t1 & t2))

def print_progression(n, l, freq=1000):
    if n % freq == 0:
        print(f"{n}/{l}    {n*100/l:.2f}%             ", end="\r")

def find_3tet(tiles, restriction):
    count = 0
    index = {}
    l = len(tiles)
    for i1, p1 in enumerate(tiles):
        count += 1
        print_progression(count, l, freq=1)
        for i2, p2 in enumerate(tiles[i1+1:]):
            si = p1+p2
            for p3 in tiles[i1+i2+2:]:
                s = si+p3
                if not restriction(s):
                    continue
                if s not in index:
                    index[s] = []
                index[s].append(frozenset((p1,p2,p3)))
        
    print(f"Added {len(index)} 3-3tets                       ")
    return index


def better_find_3tet(tiles):
    """ This methods find triplets of squares that sum to a square.
        It takes advantage of the fact that at most one of the three must be congruent to 1 modulo 4
        the rest being congruent to 0 modulo 4
        x => x^2 % 4
        0 => 0
        1 => 1
        2 => 0
        3 => 1

        0 + 0 + 0 => 0
        0 + 0 + 1 => 1
        0 + 1 + 1 => 2 which cannot be a square
        1 + 1 + 1 => 3 which cannot be a square
        """
    m4_0 = [] # tiles congruent to 0 modulo 4
    m4_1 = [] # tiles congruent to 1 modulo 4
    count = 0
    print("Grouping tiles by modulo 4")
    for t in tiles:
        if t % 4 == 0:
            m4_0.append(t)
        else:
            m4_1.append(t)
        count += 1
        print_progression(count, len(tiles), freq=1)

    m4_p0 = {} # pairs of tiles whose sum is congruent to 0 modulo 4
    print("Summing pairs of tiles congruent to 0 modulo 4")
    t = len(m4_0)*len(m4_0)-1/2
    for i1, p1 in enumerate(m4_0):
        for p2 in m4_0[i1+1:]:
            si = p1+p2
            if si not in m4_p0:
                m4_p0[si] = []
            m4_p0[p1+p2].append((p1,p2))

    index = {}
    print("Summing pairs whose sum is congruent to 0 modulo 4 with all tiles")
    # we only keep the triplets that actually are squares
    for s1, l1 in m4_p0.items():
        for p2 in tiles:
            s = s1 + p2
            if s**0.5 % 1 != 0:
                continue
            if s not in index:
                index[s] = set()
            for p1 in l1:
                if p2 == p1[0] or p2 == p1[1]:
                    continue
                index[s].add(frozenset((p1[0],p1[1],p2)))
           
    print("Removing sums that have no triplets") 
    all_keys = list(index.keys())
    for s in all_keys:
        if len(index[s]) == 0:
            del index[s]
    
    return index


def restrict_triplet(tiles, restriction, restriction_name):
    """ Find all square that can be written as a triplet of square, eg: """
    triplets = better_find_3tet(tiles)
    # triplets = find_3tet(tiles, restriction)
    restricted_triplet = {}
    count = 0
    for triplet_sum in triplets:
        count += len(triplets[triplet_sum])
        for triplet in triplets[triplet_sum]:
            for sq in triplet:
                if sq not in restricted_triplet:
                    restricted_triplet[sq] = []
                restricted_triplet[sq].append(triplet)
    print(f"{count} {restriction_name} triplet found indexed by {len(restricted_triplet)} squares")
    return restricted_triplet


def analyze_candidate(tiles, c, i0, i1, i2, i3, i4, i5, restriction, display, scount_min):
    goal = c+c+i0+i1+i2+i3+i4+i5
    
    e0 = c+i0+i3+i4+i5 # - r
    e1 = i1-i3 # + r
    e2 = c+i0+i3+i2+i5 # - r
    e3 = i1-i5 # + r
    e4 = c+i2+i3+i4+i5 # - r

    ran = range(0)
    if scount_min == 6:
        ran = tiles
    else:
        mi = - min(e1,e3)
        ma = min(e0,e2,e4)
        ran = range(mi, ma)


    for r in ran:
        scount = 0
        flag = False
        for t in [r, e0-r, e1+r, e2-r, e3+r, e4-r]:
            if restriction(t):
                scount += 1
            elif scount_min == 6:
                flag = True
                break
        if flag:
            continue

        if scount >= scount_min:
            s = [
                    r, c+i1+i2, e0-r
                , c+i0+i1, i4, i5, c+i2+i3
                , e4-r, i3,  c , i0, e1+r
                , c+i5+i0, i2, i1, c+i3+i4
                    , e3+r, c+i4+i5, e2-r
            ]

            if len(set(s)) == 19:

                print(goal, ": ", f"(scount: {scount})                           ")
                display(s)
                print()

def make_unique_id(i0, i1, i2, i3, i4, i5):
    m = min(i0, i1, i2, i3, i4, i5)
    if m == i0:
        if i1 < i5:
            return (i0, i1, i2, i3, i4, i5)
        return (i0, i5, i4, i3, i2, i1)
    if m == i1:
        if i0 < i2:
            return (i1, i0, i5, i4, i3, i2)
        return (i1, i2, i3, i4, i5, i0)
    if m == i2:
        if i1 < i3:
            return (i2, i1, i0, i5, i4, i3)
        return (i2, i3, i4, i5, i0, i1)
    if m == i3:
        if i2 < i4:
            return (i3, i2, i1, i0, i5, i4)
        return (i3, i4, i5, i0, i1, i2)
    if m == i4:
        if i3 < i5:
            return (i4, i3, i2, i1, i0, i5)
        return (i4, i5, i0, i1, i2, i3)
    if i4 < i0:
        return (i5, i4, i3, i2, i1, i0)
    return (i5, i0, i1, i2, i3, i4)



def find_restricted_triplet_sextuplet(tiles, restriction, restriction_name, display, scount_min):
    count = 0
    indexed_restricted_triplets = restrict_triplet(tiles, restriction, restriction_name)
    processes = []

    l = 0
    center_indexed_triplets = {}
    for center in indexed_restricted_triplets:
        if len(indexed_restricted_triplets[center]) < 6:
            continue
        restricted_triplets = indexed_restricted_triplets[center]
        center_indexed_triplets[center] = {}
        for _, triplet in enumerate(restricted_triplets):
            p1, p2 = tuple(triplet - {center})
            if p1 not in center_indexed_triplets[center]:
                center_indexed_triplets[center][p1] = []
            center_indexed_triplets[center][p1].append(p2)
            if p2 not in center_indexed_triplets[center]:
                center_indexed_triplets[center][p2] = []
            center_indexed_triplets[center][p2].append(p1)
        l += len(center_indexed_triplets[center])

    centers = list(center_indexed_triplets.keys())
    centers.sort()
    for center in centers:
        index = set() # index of unique center hexagon
        indexed_triplets = center_indexed_triplets[center]
        indexed_triplets_keys = list(indexed_triplets.keys())
        indexed_triplets_keys.sort()
        for s0 in indexed_triplets_keys:
            count += 1
            for s1 in indexed_triplets[s0]:
                print(f"center: {center: 10d}, s0: {s0: 10d}, unique_index: {len(index): 10d}", end=" ")
                print_progression(count, l, freq=1)
                for s2 in indexed_triplets[s1]:
                    if s2 == s0:
                        continue
                    for s3 in indexed_triplets[s2]:
                        if s3 == s0 or s3 == s1:
                            continue
                        for s4 in indexed_triplets[s3]:
                            if s4 == s0 or s4 == s1 or s4 == s2:
                                continue
                            for s5 in indexed_triplets[s4]:
                                if s5 == s0 or s5 == s1 or s5 == s2 or s5 == s3:
                                    continue

                                if len(frozenset((center, s0,s1,s2,s3,s4,s5))) != 7:
                                    continue
                                inner = make_unique_id(s0,s1,s2,s3,s4,s5)
                                if inner in index: # deduplicate result to avoid rotation and mirroring
                                    continue
                                index.add(inner)
                                if restriction(center+s5+s0):
                                    p = multiprocessing.Process(target=analyze_candidate, args=(tiles, center, s0,s1,s2,s3,s4,s5, restriction, display, scount_min)).start()
                                    processes.append(p)
                                    if len(processes) >= 8:
                                        for p in processes:
                                            if p is not None:
                                                p.join()
                                        processes = []
    for p in processes:
        if p is not None:
            p.join()

def prepare_display(l, s="  "):
    return [
            s*3 + s.join(l[0:3]) + s*3,
            s*2 + s.join(l[3:7]) + s*2,
            s + s.join(l[7:12]) + s,
            s*2 + s.join(l[12:16]) + s*2,
            s*3 + s.join(l[16:19]) + s*3
        ]

def find_square_hex(max):

    def is_square(n):
        """ Return true if n is the square of an integer, false otherwise """
        return n >= 0 and n **0.5 % 1 == 0
    
    def display(s, color="white"):
        if len(s) != 19:
            raise Exception("Solution must be 19 positions")
        l = [f'{(int(i**0.5)): 3d}²' if is_square(i) else f'{i: 4d}' for i in s]
        print("\n".join(prepare_display(l, "  ")))

    tiles = [(i+1)**2 for i in range(max)]
    t0 = time.time()
    find_restricted_triplet_sextuplet(tiles, is_square, "square", display, scount_min=6)
    print(f"\ntook {time.time()-t0:.2f} seconds                       ")


find_square_hex(1000)
