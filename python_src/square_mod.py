import itertools
import sys

m = 4
if len(sys.argv) > 1:
    m = int(sys.argv[1])

square_mod = []
available_numbers = []
for i in range(m):
    r = i**2 % m
    if r not in square_mod:
        square_mod.append(r)

for t in itertools.product(square_mod, repeat=3):
    r = sum(t) % m
    if r in square_mod and r not in available_numbers:
        available_numbers.append(r)

print(f"Modulo {m}:", square_mod)
print(f"Modulo triplet {m}:", available_numbers)

def prepare_print(s_sum, c, x, t):
    spacing = 2
    lines = [
        (" "*spacing).join([f"{i:02d}" for i in       [t[0], x[0], t[1]]]),
        (" "*spacing).join([f"{i:02d}" for i in    [x[5], c[1], c[2], x[1]]]),
        (" "*spacing).join([f"{i:02d}" for i in [t[5], c[6], c[0], c[3], t[2]]]),
        (" "*spacing).join([f"{i:02d}" for i in    [x[4], c[5], c[4], x[2]]]),
        (" "*spacing).join([f"{i:02d}" for i in       [t[4], x[3], t[3]]])
    ]
    result = " "*spacing*4 + f"-{s_sum:02d}-" + " "*(spacing*5-2) + "\n"
    result += " "*spacing*3 + lines[0] + " "*spacing*3 + "\n"
    result += " "*spacing*2 + lines[1] + " "*spacing*2 + "\n"
    result += " "*spacing*1 + lines[2] + " "*spacing*1 + "\n"
    result += " "*spacing*2 + lines[3] + " "*spacing*2 + "\n"
    result += " "*spacing*3 + lines[4] + " "*spacing*3
    return result
#     return f"""
#      -{s_sum}-   
#     {t[0]: 2d} {x[0]: 2d} {t[1]: 2d}  
#    {x[5]} {c[1]} {c[2]} {x[1]} 
#   {t[5]} {c[6]} {c[0]} {c[3]} {t[2]}
#    {x[4]} {c[5]} {c[4]} {x[2]} 
#     {t[4]} {x[3]} {t[3]}  
#           """

potential_solution = set()

def rotate(l):
    return tuple([l[0], l[2], l[3], l[4], l[5], l[6], l[1]])

def rot(l, n):
    for _ in range(n):
        l = rotate(l)
    return tuple(l)

def flip(l):
    return tuple([l[0], l[2], l[1], l[6], l[5], l[4], l[3]])

for s in itertools.product(available_numbers, repeat=7):
    check = [
        (0,1,2),(0,2,3),(0,3,4),(0,4,5),(0,5,6),(0,6,1), 
    ]
    flag = False
    for c in check:
        if (s[c[0]] + s[c[1]] + s[c[2]]) % m not in available_numbers:
            flag = True
            break
    if not flag:
        f = flip(s)
        rot_flag = False
        #print(potential_solution)
        for t in [rot(s, i) for i in range(7)] + [rot(f, i) for i in range(7)]:
            #print(t, t in potential_solution)
            if t in potential_solution:
                rot_flag = True
                break
        if not rot_flag:
            potential_solution.add(s)

solutions = set()

for p in potential_solution:

    x = (
        (p[0] + p[4] + p[5]) % m,
        (p[0] + p[5] + p[6]) % m,
        (p[0] + p[6] + p[1]) % m,
        (p[0] + p[1] + p[2]) % m,
        (p[0] + p[2] + p[3]) % m,
        (p[0] + p[3] + p[4]) % m,
    )

    s_sum = (sum(p) + p[0]) % m
    t = [0,0,0,0,0,0]

    for t0 in available_numbers:
        t1 = (s_sum + m - t0 - x[0]) % m
        t5 = (s_sum + m - t0 - x[5]) % m
        if t1 not in available_numbers or t5 not in available_numbers:
            continue
        t2 = (s_sum + m - t1 - x[1]) % m
        t4 = (s_sum + m - t5 - x[4]) % m
        if t2 not in available_numbers or t4 not in available_numbers:
            continue
        if s_sum != (t2 + t5 + p[0] + p[3] + p[6]) % m:
            continue
        if s_sum != (t4 + t1 + p[0] + p[2] + p[5]) % m:
            continue
        t3_1 = (s_sum + m - t2 - x[2]) % m
        t3_2 = (s_sum + m - t4 - x[3]) % m
        if t3_1 != t3_2 or t3_1 not in available_numbers:
            continue
        if s_sum != (t3_1 + t0 + p[0] + p[1] + p[4]) % m:
            continue

        solutions.add((s_sum, p, x, (t0, t1, t2, t3_1, t4, t5)))

full_lines = ["" for _ in range(8)]
count = 0
for s_sum, p, ext, t in solutions:
    # print(prepare_print(p, ext))
    
    lines = prepare_print(s_sum, p, ext, t).split("\n")
    for i, l in enumerate(lines):
        full_lines[i] += l
    count += 1
    if count == 9:
        print("\n".join(full_lines))
        count = 0
        full_lines = ["" for _ in range(8)]

if full_lines[0] != "":
    print("\n".join(full_lines))


print(f"Square Modulo {m}:", available_numbers)
print(f"Square Modulo triplet {m}:", available_numbers)
print("Solutions:", len(solutions))
