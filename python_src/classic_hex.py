import time

def prepare_solution_for_printing(s):
    if len(s) != 19:
        raise Exception("Solution must be 19 positions")
    l = [f'{(i):02d}' for i in s]
    return ["      " + "  ".join(l[0:3]) + "      ",
            "    " + "  ".join(l[3:7]) + "    ",
            "  " + "  ".join(l[7:12]) + "  ",
            "    " + "  ".join(l[12:16]) + "    ",
            "      " + "  ".join(l[16:19]) + "      "
    ]

def display_solution(s):
    if len(s) != 19:
        raise Exception("Solution must be 19 positions")
    print("\n".join(prepare_solution_for_printing(s)))


def solve_magic_hex(tiles):
    solutions = []
    r0 = frozenset(tiles)
    for A in r0:
        doneBC = set()
        r1 = r0 - {A}
        for B in r1:
            r2 = r1 - {B}
            for C in r2:
                if (B,C) in doneBC or (C,B) in doneBC:
                    continue
                doneBC.add((B,C))
                doneBC.add((C,B))
                r3 = r2 - {C}
                ABC = A+B+C
                if not ABC in r3:
                    continue
                r4 = r3 - {ABC}
                for D in r4:
                    if (C,D) in doneBC or (D,C) in doneBC:
                        continue
                    r5 = r4 - {D}
                    ACD = A+C+D
                    if not ACD in r5:
                        continue
                    r6 = r5 - {ACD}
                    for G in r6:
                        if (G,B) in doneBC or (B,G) in doneBC:
                            continue
                        r7 = r6 - {G}
                        ABG = A+B+G
                        if not ABG in r7:
                            continue
                        r8 = r7 - {ABG}
                        for H in r8:
                            r9 = r8 - {H}
                            ABCDGH = A+B+C+D+G-H
                            if not ABCDGH in r9:
                                continue
                            r10 = r9 - {ABCDGH}
                            for F in r10:
                                if (G,F) in doneBC or (F,G) in doneBC:
                                    continue
                                r11 = r10 - {F}
                                AFG = A+F+G
                                if not AFG in r11:
                                    continue
                                r12 = r11 - {AFG}
                                ABCFGH = A+B+C+F+G-H
                                if not ABCFGH in r12:
                                    continue
                                r13 = r12 - {ABCFGH}
                                ACDFGH = A+C+D+F+G-H
                                if not ACDFGH in r13:
                                    continue
                                r14 = r13 - {ACDFGH}
                                for E in r14:
                                    if (E,F) in doneBC or (F,E) in doneBC or (E,D) in doneBC or (D,E) in doneBC:
                                        continue
                                    r15 = r14 - {E}
                                    AEF = A+E+F
                                    if not AEF in r15:
                                        continue
                                    r16 = r15 - {AEF}
                                    ADE = A+D+E
                                    if not ADE in r16:
                                        continue
                                    r17 = r16 - {ADE}
                                    EHC = E+H-C
                                    if not EHC in r17:
                                        continue
                                    r18 = r17 - {EHC}
                                    EHG = E+H-G
                                    if not EHG in r18:
                                        continue
                                    solution = [H, AEF, ABCDGH, ADE, B, C, AFG, ABCFGH, G, A, D, EHG, ACD, F, E, ABG, EHC, ABC, ACDFGH]
                                    solutions.append(solution)
                                    display_solution(solution)
                                    print("\n")
    return solutions


def solve_classic_magic_hex(offset=1):
    timing_number = 1
    tiles = [(i+offset) for i in range(19)]
    t0 = time.time()
    solution = []
    for i in range(timing_number):
        solution = solve_magic_hex(tiles)
    print(f"ran {timing_number} times: iteration took {(time.time()-t0)/timing_number:.2f} seconds on average")
    print(f"found {len(solution)} solutions for offset {offset}")

# solve_classic_magic_hex(-24)
# solve_classic_magic_hex(-19)
# solve_classic_magic_hex(-14)
# solve_classic_magic_hex(-9)
solve_classic_magic_hex(1)
# solve_classic_magic_hex(1)
# solve_classic_magic_hex(6)
# solve_classic_magic_hex(11)
# solve_classic_magic_hex(16)