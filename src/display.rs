fn prepare_solution_for_printing(s: &[i32; 19]) -> Vec<String> {
    if s.len() != 19 {
        panic!("Solution must be 19 positions");
    }
    let l: Vec<String> = s.iter().map(|&i| format!("{:02}", i)).collect();
    vec![
        format!("      {}  {}  {}", l[0], l[1], l[2]),
        format!("    {}  {}  {}  {}", l[3], l[4], l[5], l[6]),
        format!("  {}  {}  {}  {}  {}", l[7], l[8], l[9], l[10], l[11]),
        format!("    {}  {}  {}  {}", l[12], l[13], l[14], l[15]),
        format!("      {}  {}  {}", l[16], l[17], l[18]),
    ]
}

pub fn display_solution(s: &[i32; 19]) {
    if s.len() != 19 {
        panic!("Solution must be 19 positions");
    }
    for line in prepare_solution_for_printing(s).iter() {
        println!("{}", line);
    }
}