// use std::env;
use std::time::Instant;

mod bitset;
// mod classic_hex;
// mod display;
mod square_hex;
// use crate::classic_hex::solve_classic_magic_hex;
use crate::square_hex::solve_magic_hex;


fn main() {
    // let args: Vec<String> = env::args().collect();
    // if args.len() > 1 {
    //     let offset = args[1].parse::<i32>().unwrap();
    //     solve_classic_magic_hex(offset, 1);
    // } else {
    //     println!("Please provide an offset as a command line argument.");
    // }
    const SIZE: usize = 10000;
    let mut tiles: [i32; SIZE] = [0; SIZE];
    for i in 0..SIZE {
        tiles[i] = (i as i32)*(i as i32);
    }
    let t0 = Instant::now();
    solve_magic_hex(tiles.to_vec(), 0, None);
    println!("finished in {:.4} seconds", t0.elapsed().as_secs_f64());
}
