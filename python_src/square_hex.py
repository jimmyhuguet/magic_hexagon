import time
import sys

def prepare_solution_for_printing(s):
    if len(s) != 19:
        raise Exception("Solution must be 19 positions")
    l = []
    for i in s:
        if i < 0 or i ** 0.5 != int(i ** 0.5):
            l.append(f'{i:02d}')
        else:
            l.append(f'{int(i**0.5):02d}²')
    return ["      " + "  ".join(l[0:3]) + "      ",
            "    " + "  ".join(l[3:7]) + "    ",
            "  " + "  ".join(l[7:12]) + "  ",
            "    " + "  ".join(l[12:16]) + "    ",
            "      " + "  ".join(l[16:19]) + "      "
    ]

def display_solution(s):
    if len(s) != 19:
        raise Exception("Solution must be 19 positions")
    print("\n".join(prepare_solution_for_printing(s)))


def print_progression(n, l, freq=1000):
    if n % freq == 0:
        print(f"{n}/{l}    {n*100/l:.2f}%             ", end="\r")

def find_3tet_with_given_center(center, m4_0, m4_1):
    """ This methods find triplets of squares that sum to a square.
        It takes advantage of the fact that at most one of the three must be congruent to 1 modulo 4
        the rest being congruent to 0 modulo 4
        x => x^2 % 4
        0 => 0
        1 => 1
        2 => 0
        3 => 1

        0 + 0 + 0 => 0
        0 + 0 + 1 => 1
        0 + 1 + 1 => 2 which cannot be a square
        1 + 1 + 1 => 3 which cannot be a square
        """
    
    def fill_index(index, p2, p3):
        def _inner_add(index, p2, p3):
            if p2 not in index:
                index[p2] = set()
            index[p2].add(p3)

        _inner_add(index, p2, p3)
        _inner_add(index, p3, p2)
    
    index = {}

    if center % 4 == 0:
        for i2, p2 in enumerate(m4_0):
            for p3 in m4_0[i2+1:]:
                s = center + p2 + p3
                if s**0.5 % 1 == 0:
                    fill_index(index, p2, p3)
            for p3 in m4_1:
                s = center + p2 + p3
                if s**0.5 % 1 == 0:
                    fill_index(index, p2, p3)
    else:
        for i2, p2 in enumerate(m4_0):
            for p3 in m4_0[i2+1:]:
                s = center + p2 + p3
                if s**0.5 % 1 == 0:
                    fill_index(index, p2, p3)
    return index

def is_square(n):
    if n < 0:
        return False
    return n**0.5 % 1 == 0

def solve_magic_hex_with_given_center(r0, tiles_0, tiles_1, A):
    index = find_3tet_with_given_center(A, list(tiles_0 - {A}), list(tiles_1 - {A}))
    doneBC = set()
    for ib, B in enumerate(sorted(index.keys())):
        for C in index[B]:
            if (B,C) in doneBC or (C,B) in doneBC:
                continue
            print(f"A: {int(A**0.5)}²,  B: {int(B**0.5)}²  ({ib}/{len(index)}),   C: {int(C**0.5)}²                ", end="\r")
            doneBC.add((B,C))
            doneBC.add((C,B))
            ABC = A+B+C
            if not is_square(ABC):
                continue
            for D in index[C]:
                if (C,D) in doneBC or (D,C) in doneBC:
                    continue
                ACD = A+C+D
                if not is_square(ACD):
                    continue
                for E in index[D]:
                    if (D,E) in doneBC or (E,D) in doneBC:
                        continue
                    ADE = A+D+E
                    if not is_square(ADE):
                        continue
                    for F in index[E]:
                        if (E,F) in doneBC or (F,E) in doneBC:
                            continue
                        AEF = A+E+F
                        if not is_square(AEF):
                            continue
                        for G in index[B]:
                            if (G,B) in doneBC or (B,G) in doneBC or (G,F) in doneBC or (F,G) in doneBC:
                                continue
                            ABG = A+B+G
                            if not is_square(ABG):
                                continue
                            AGF = A+G+F
                            if not is_square(AGF):
                                continue
                            used = {A,B,C,D,E,F,G,ABC,ACD,ADE,AEF,ABG,AGF}
                            if len(used) != 13:
                                continue
                            remaining = r0 - used
                            for H in remaining:
                                count = 6
                                ABCDGH = A+B+C+D+G-H
                                if not is_square(ABCDGH):
                                    count -= 1
                                EHG = E+H-G
                                if not is_square(EHG):
                                    count -= 1
                                ABCFGH = A+B+C+F+G-H
                                if not is_square(ABCFGH):
                                    count -= 1
                                EHC = E+H-C
                                if not is_square(EHC):
                                    count -= 1
                                ACDFGH = A+C+D+F+G-H
                                if not is_square(ACDFGH):
                                    count -= 1
                                if count < 5:
                                    continue
                                used = used | {H, ABCDGH, EHG, ABCFGH, EHC, ACDFGH}
                                if count < 6 and len(used) < 19:
                                    continue
                                print(f"\n Count = {count}, unique: {len(used)}")
                                display_solution([H, AEF, ABCDGH, ADE, B, C, AGF, ABCFGH, G, A, D, EHG, ACD, F, E, ABG, EHC, ABC, ACDFGH])
                                if count == 6 and len(used) == 19:
                                    return [H, AEF, ABCDGH, ADE, B, C, AGF, ABCFGH, G, A, D, EHG, ACD, F, E, ABG, EHC, ABC, ACDFGH]
    return []

def solve_magic_hex(tiles, start_A=0, end_A=None):
    tiles_0 = set()
    tiles_1 = set()
    for t in tiles:
        if t % 4 == 0:
            tiles_0.add(t)
        else:
            tiles_1.add(t)
    r0 = frozenset(tiles)
    for A in tiles:
        if A**0.5 < start_A:
            continue
        if end_A is not None and A**0.5 > end_A:
            print(f"A: {int(A**0.5)}² => Stopped.                       ")
            break
        print(f"A: {int(A**0.5)}² => Preparing index                                                 ", end="\r")
        solution = solve_magic_hex_with_given_center(r0, tiles_0, tiles_1, A)
        if len(solution) > 0:
            return solution
    return []



def solve_square_magic_hex():
    timing_number = 1
    tiles = [(i**2) for i in range(0,3000)]
    t0 = time.time()
    solution = []
    for i in range(timing_number):
        start_A = 0
        end_A = None
        if len(sys.argv) > 1:
            start_A = int(sys.argv[1])
        if len(sys.argv) > 2:
            end_A = int(sys.argv[2])
        solution = solve_magic_hex(tiles, start_A, end_A)
    print(f"ran {timing_number} times: iteration took {(time.time()-t0)/timing_number:.2f} seconds on average\n")
    if len(solution) == 0:
        print("No solution found")
    else:
        display_solution(solution)

solve_square_magic_hex()
