#[derive(Copy, Clone, Eq, PartialEq)]
pub struct BitSet {
    v: u64,
}

impl BitSet {
    fn new() -> Self {
        BitSet {
            v: 0,
        }
    }

    pub fn insert(&mut self, v: i32) {
        self.v |= 1 << v;
    }

    pub fn contains(&self, v: &i32) -> bool {
        v >= &0 && self.v & (1 << v) != 0
    }

    pub fn difference(&self, other: &BitSet) -> Self {
        BitSet {
            v: self.v & (!other.v),
        }
    }

    pub fn from<I: IntoIterator<Item = i32>>(it: I) -> Self {
        let mut ret = BitSet::new();
        for v in it {
            ret.insert(v);
        }
        ret
    }
}

pub struct BitSetIter {
    v: u64,
}

impl IntoIterator for &BitSet {
    type Item = i32;
    type IntoIter = BitSetIter;
    fn into_iter(self) -> BitSetIter {
        BitSetIter {
            v: self.v,
        }
    }
}

impl IntoIterator for BitSet {
    type Item = i32;
    type IntoIter = BitSetIter;
    fn into_iter(self) -> BitSetIter {
        (&self).into_iter()
    }
}

impl Iterator for BitSetIter {
    type Item = i32;
    fn next(&mut self) -> Option<Self::Item> {
        if self.v == 0 {
            return None;
        }
        let zeros = self.v.trailing_zeros();
        let ret = Some(zeros as i32);
        self.v ^= 1 << zeros;
        ret
    }
}