use std::collections::BTreeSet;
use std::time::Instant;

use rayon::prelude::*;
use rayon::iter::ParallelBridge;

use crate::bitset::BitSet;
use crate::display::display_solution;

fn solve_magic_hex(tiles: &[i32; 19]) ->  Vec<[i32; 19]> {
    // Init 
    let r0 = BitSet::from(tiles.iter().copied());
    r0.into_iter().par_bridge().flat_map(|a| {
        let mut solutions = Vec::new();
        let mut done_bc = BTreeSet::new();
        let r1: BitSet = r0.difference(&BitSet::from([a]));
        for b in &r1 {
            let r2: BitSet = r1.difference(&BitSet::from([b]));
            for c in &r2 {
                if done_bc.contains(&(b, c)) || done_bc.contains(&(c, b)) {
                    continue;
                }
                done_bc.insert((b, c));
                done_bc.insert((c, b));
                let r3: BitSet = r2.difference(&BitSet::from([c]));
                let abc = a+b+c;
                if !r3.contains(&abc) {
                    continue;
                }
                let r4: BitSet = r3.difference(&BitSet::from([abc]));
                for d in &r4 {
                    if done_bc.contains(&(c, d)) || done_bc.contains(&(d, c)) {
                        continue;
                    }
                    let r5: BitSet = r4.difference(&BitSet::from([d]));
                    let acd = a+c+d;
                    if !r5.contains(&acd) {
                        continue;
                    }
                    let r6: BitSet = r5.difference(&BitSet::from([acd]));
                    for g in &r6 {
                        if done_bc.contains(&(b, g)) || done_bc.contains(&(g, b)) {
                            continue;
                        }
                        let r7: BitSet = r6.difference(&BitSet::from([g]));
                        let abg = a+b+g;
                        if !r7.contains(&abg) {
                            continue;
                        }
                        let r8: BitSet = r7.difference(&BitSet::from([abg]));
                        for h in &r8 {
                            let r9: BitSet = r8.difference(&BitSet::from([h]));
                            let abcdgh = a+b+c+d+g-h;
                            if !r9.contains(&abcdgh) {
                                continue;
                            }
                            let r10: BitSet = r9.difference(&BitSet::from([abcdgh]));
                            for f in &r10 {
                                if done_bc.contains(&(g, f)) || done_bc.contains(&(f, g)) {
                                    continue;
                                }
                                let r11: BitSet = r10.difference(&BitSet::from([f]));
                                let afg = a+f+g;
                                if !r11.contains(&afg) {
                                    continue;
                                }
                                let r12: BitSet = r11.difference(&BitSet::from([afg]));
                                let abcfgh = a+b+c+f+g-h;
                                if !r12.contains(&abcfgh) {
                                    continue;
                                }
                                let r13: BitSet = r12.difference(&BitSet::from([abcfgh]));
                                let acdfgh = a+c+d+f+g-h;
                                if !r13.contains(&acdfgh) {
                                    continue;
                                }
                                let r14: BitSet = r13.difference(&BitSet::from([acdfgh]));
                                for e in &r14 {
                                    if done_bc.contains(&(e, f)) || done_bc.contains(&(f, e)) || done_bc.contains(&(e, d)) || done_bc.contains(&(d, e)) {
                                        continue;
                                    }
                                    let r15: BitSet = r14.difference(&BitSet::from([e]));
                                    let aef = a+e+f;
                                    if !r15.contains(&aef) {
                                        continue;
                                    }
                                    let r16: BitSet = r15.difference(&BitSet::from([aef]));
                                    let ade = a+d+e;
                                    if !r16.contains(&ade) {
                                        continue;
                                    }
                                    let r17: BitSet = r16.difference(&BitSet::from([ade]));
                                    let ehc = e+h-c;
                                    if !r17.contains(&ehc) {
                                        continue;
                                    }
                                    let r18: BitSet = r17.difference(&BitSet::from([ehc]));
                                    let ehg = e+h-g;
                                    if !r18.contains(&ehg) {
                                        continue;
                                    }
                                    let solution = [h, aef, abcdgh, ade, b, c, afg, abcfgh, g, a, d, ehg, acd, f, e, abg, ehc, abc, acdfgh];
                                    solutions.push(solution);
                                    display_solution(&solution);
                                    println!("\n");
                                }
                            }
                        }
                    }
                }
            }
        }
        solutions
    }).collect()
}

pub fn solve_classic_magic_hex(offset: i32, timing_count: usize) {
    let mut tiles: [i32; 19] = [0; 19];
    for i in 0..19 {
        tiles[i] = offset + i as i32;
    }
    let mut solution: Vec<[i32; 19]> = Vec::new();
    let t0 = Instant::now();
    for _ in 0..timing_count {
        solution = solve_magic_hex(&tiles);
    }
    println!("ran {} times: iteration took {:.4} seconds on average", timing_count, t0.elapsed().as_secs_f64() / timing_count as f64);
    println!("found {} solutions for offset {}", solution.len(), offset);
}
